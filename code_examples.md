examples of Meteor code packages
----
* Jade
    * Meteor pacreach:jade (from dalgard:jade & mquandalle:jade)
        * client\pages\examples\preferences.jade
            * +leaderboard   ( {{> leaderboard}} in HTML) - include a template
            * \#{expression} -or- {{expression}}
            * = expression
        * client\pages\examples\pref_question.tpl.jade
            * name.tpl.jade  - auto template naming
        * no examples yet
            * !{jade} - raw HTML
            * input($dyn = attrs) ( <input {{attrs}}> in HTML (Spacebars) ) - include a list of attributes from *attrs*
            * the "+ is options for *if*, *unless*, *each*, and *with*
            * *else* and *else if*
        * from dalgard:jade
            * | Hello #{person(name prefix='Lord')} - arguments
            * input(type='text' placeholder=person(name prefix='Lord')) - arguments
            * input($attr) == input($dyn = attr)
            * attr='#{@index}'  - access the special @index for *each* loop
        * from mquandalle
            * components
```jade
            //- First, define the component (jade + js)
            template(name="ifEven")
              if isEven value
                +UI.contentBlock
              else
                +UI.elseBlock

            //-
              Template.ifEven.isEven = function (value) {
                return value % 2 === 0;
              };
            //- And now we can use it!
            body
              +ifEven(value=2)
                | 2 is even
              else
            | 2 is odd
```

end text
