Teachers' Brigade
---
Tech notes and history
---
* 2/28/2017 - added *Jade* ( *Pug* ), *Stylus*, and *CoffeeScript*
* 3/1/2017 - removed Materialize, added nav bar & dummy pages
* 3/15/2017 - start of sign-up page: questions with checkboxes
* 3/22/2017 - added all fields to sign-up page
* 3/24/2017 - all check boxes work correctly on sign-up page
