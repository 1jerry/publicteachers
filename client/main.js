// Startup
import "/imports/startup/client";

// Routes
import "/imports/api/routes";

// Library
import "/imports/library/blazeHelpers";
import "/imports/library/myBlazeHelpers";
import "/imports/library/VMmixins";

// Modules
import "/imports/modules/reactive-window";
import "/imports/modules/ddpLogging";

// Pages, layouts and components
import "/imports/ui/layouts/mainLayout";
import "/imports/ui/pages/home";

// Stylesheets
import "/imports/ui/stylesheets/main.css";

// Methods
import "/imports/api/methods";

// Collections
import "/imports/api/collections";
import "/imports/api/collections/helpers";
import "/imports/api/collections/client";
