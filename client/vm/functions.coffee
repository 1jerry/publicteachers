Template.TFquestion.viewmodel
  text: ''
#  isChecked: false
  click: ->
    @isChecked(!@isChecked())
  changed: ->
    @parent().hasChanged(true)

Template.pref_question.viewmodel
  share: 'person'
  text: ''
  tText: 'I would like a volunteer to'
  vText: 'I can'
  box: 'unspecified'
  skipNT: 0
  isChecked: false
  onRendered: ->
    Meteor.setTimeout =>
      @isChecked !!@boxes()[@box()]
#      console.log "onRendered, checked, boxes = ", @isChecked(), @boxes() if @box() is 'retired'
    , 100
  partOne: ->
    if @isTeacher()
      @tText()
    else
      @vText()
  ifSkip: ->
    @nonTeacher() and @skipNT()
  click: ->
    @isChecked(!@isChecked())
    @boxes()[@box()] = @isChecked()

Template.question.viewmodel
  text: ''
  ph: ''
  value: ''
  ref: ''
  fld: ''
  name: ''
  lock: false
  persist: false  # while designing
  autorun: ->
    @parent()[@name()] @fld() if @name()

Template.link.viewmodel
  text: ''
  name: 'missing'
  display: ->
    @text() or @name().substr(0,1).toUpperCase() + @name().substr(1)
  path: ->
    Blaze._globalHelpers["pathFor"](@name())
  active: ->
    Blaze._globalHelpers["isActiveRoute"](@name())
