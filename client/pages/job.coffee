import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.job.viewmodel
  mixin: 'job'
  share: 'new' : 'thisTeacher'
  focus1: true
  editMode: true
  fields: [
    'jobOwner'
  ]
  field: (name) ->
    @[name.split(',')[0]]?()
  title: (name) ->
    [field, title] = name.split ',',2
    title or field.substr(0,1).toUpperCase() + field.substr(1)
  tasks: ->
    trecs = Tasks.find({},
      sort:
        code:1
    ).fetch()
    fix = (i) -> i.jobname = i.code + " - " + i.name
    fix item for item in trecs
    trecs
  updateText: ->
    if @_id?() then "Update" else "Create"
  signal: 'keyboard'
  autorun: [
    ->
      key = @keyDown().which
      @editMode false if key is 27
    ->
      history.back() unless @editMode()
    ->
      if @taskId()
        @jobName(@xTask().name) unless @jobName.value and @_id()
        @jobDescription(@xTask().description) unless @jobDescription.value and @_id()
  ]
  cancel: ->
    @editMode false

  canUpdate: ->
    @jobName.valid() and (@_id() or @teacherId() is @new.teacherId())
  onCreated: (tmpl) ->
    tmpl.subscribe 'tasks'
    tmpl.subscribe 'me', =>
      @teacherId @new.teacherId()
#      console.log "TMP:job:onCreated: begin teacherId ", @teacherId()
      id = Router.getParam('id')
      data = Jobs.findOne id if id
      if data
        ## handle collection helpers
        @jobOwner data.jobOwner()
        delete data.jobOwner
        @load data
#      console.log "TMP:job:onCreated: end teacherId ", @teacherId()
      return
  events:
    'submit form': (e) ->
      e.preventDefault()
      @update()
  update: ->
    if @canUpdate()
      data = _.pick @data(), 'jobName jobDescription teacherId taskId'.split(' ')
      data.isActive = true
      if @_id?()
        Meteor.call 'jobs.update', @_id(), data, (err) =>
          if err
            Bert.alert "Could not update:<br>" + err, "danger"
          else
            Bert.alert "Updated", "success"
            @editMode(false)
      else
        Jobs.insert data, (err) =>
#        Meteor.call 'jobs.update', @_id(), data, (err) =>
          if err
            Bert.alert "Could not add:<br>" + err, "danger"
          else
            Bert.alert "Added", "success"
            @editMode(false)
