#noinspection JSUnresolvedVariable
Template.contacts.viewmodel
  share: 'edited'
  mixin: 'table'
  onRendered: -> @showContacts()
  usersColl: ->
    @noun() is 'Users'
  TVType: ->
    @noun() in ['Teachers','Volunteers']
  editText: ->
    last = -1
    word = @noun()?.slice 0, last
    if @editMode() and @sid() then "Edit #{word}" else "Add new #{word}"
  editClick: ->
    @sid("") unless @editMode()
    @editMode(true)
  showContacts: ->
    @sid("")
    @editMode(false)
