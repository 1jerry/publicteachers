import { FlowRouter } from 'meteor/ostrio:flow-router-extra';
import { isAdmin } from '/imports/library/contextUtilities'
#todo - delete
Template.users.viewmodel
  onRendered: ->
    Meteor.subscribe('main')
  userRows: ->
    Meteor.users.find()
Template.userRow.viewmodel
  mixin: 'person'
  onRendered: ->
    @load( @profile?())
    @email( @emails()[0].address)
  onClick: ->
    FlowRouter.go 'profile', id:@_id()
  preferences: ->
    bx = @profile?().boxes
    _.filter(Object.keys(bx), (o) ->
      o.startsWith('q') and bx[o]
    ).join ' ' if !!bx
  allRoles: ->
    if @roles() then @roles().join(' ') else "<none>"
  userType: ->
    if @isTeacher() then 'Teacher' else 'Volunteer' if @isVolunteer()
  isAdmin: ->
    if isAdmin @_id() then 'admin'
