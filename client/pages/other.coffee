import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

Template.loginPage.viewmodel
  onRendered: ->
    if not Meteor.userId()
      Session.set('Meteor.loginButtons.dropdownVisible',true)

Template.footer.viewmodel
  top: (e) ->
    e.stopPropagation()
    scrollSpeed = 400
    $("html, body").animate {scrollTop:0}, scrollSpeed
