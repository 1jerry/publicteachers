import {Router} from '/imports/library/default'

Template.listVolunteers.viewmodel

  find: {
    isActive: true
    isTestData: false
  }
  onRendered: ->
    Meteor.subscribe('volunteers')
  items: ->
    find = @find()
    Volunteers.find find, sort: createdAt: 1
