import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.editTask.viewmodel
  mixin: [
    'table'
    'task'
  ]
  share: [
    'edited'
  ]
  id: ''  #passed
  hasChanged: false

  getId: ->
    @_id @id() or Router.getParam('id')
  style:
    btn3:
      'margin-top': '15px'
  signal: 'keyboard'
  autorun: [
    ->
      key = @keyDown().which
      @editMode false if key is 27
    ->
      if @templateInstance.subscriptionsReady()
        @item = Tasks.findOne @getId()
        @load @item
  ]
  cancel: ->
    @editMode false
  validID: (code) ->
    if @code.valid()
      search =
        code: @code().toLowerCase()
      if item = Tasks.findOne(search)
        unless item._id is @_id()
          return false
      true
  onCreated: (tmpl) ->
    tmpl.subscribe 'all'
  updateText: ->
    if @_id() then "Update Information" else "Create"
  canUpdate: ->
    @name.valid() and @validID(@code)
  allowInactive: ->
    @.id() and @._id() isnt Meteor.userId()
  events:
    'submit form': (e) ->
      e.preventDefault()
      @update()
  update: ->
    return if not @canUpdate()
    data =
      code: @code().toLowerCase()
      name: @name()
      description: @description()
      isActive: @isActive()
      isTestData: @isTestData()
    console.log "TMP: editTask, data = ", data
    if @_id()
      Meteor.call 'tasks.update', @_id(), data, (err) =>
        if err
          Bert.alert "Could not update:<br>" + err, "danger"
        else
          Bert.alert "Updated", "success"
          @editMode false
        return
    else
      Meteor.call 'tasks.insert', data, (err, id) =>
        if err
          Bert.alert "Could not create:<br>" + err.reason, "danger"
        else
          Bert.alert "Created", "success"
          @editMode false
        return
