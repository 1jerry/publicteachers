import {FlowRouter} from 'meteor/ostrio:flow-router-extra';


ViewModel.addAttributeBinding [
  'placeholder'
  'src'
]
Accounts.ui.config
  passwordSignupFields: 'USERNAME_AND_EMAIL'

Meta.config options:
  title: "Teacher's Brigade (test)"
  suffix: "@ Teacher's Brigade (test)"

FlowRouter.wait()

Tracker.autorun ->
  if Roles.subscription.ready() and not FlowRouter._initialized
    FlowRouter.initialize()

Bert.defaults =
  style: 'growl-top-right'
  type: 'info'
  hideDelay: 4000

###
  jQuery to monitor screen position
###
$(window).scroll ->
  pxShow = 90
  fadeInTime = 400
  fadeOutTime = 800
#  replace with CSS transitions?
  if $(window).scrollTop() >= pxShow
    if $("#gototop").hasClass('hidden')   and  !$("#gototop").hasClass('animating')
      $("#gototop").transition 'browse in', fadeInTime
  else
    if $("#gototop").hasClass('visible') and  !$("#gototop").hasClass('animating')
      $("#gototop").transition 'fade out', fadeOutTime
#
