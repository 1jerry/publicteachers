import {Router} from '/imports/library/default'


#noinspection JSUnresolvedVariable
Template.test.viewmodel
  mixin: [
    'email'
    'table'
  ]
  share: [
    'edited'
    'nameAdd'
  ]
  username: ''
  name: ''
  number: ''
  email: ''
  userType: ''
  usernameDisabled: true
  validUser: true
  hasChanged: false

  active: ->
    not @notActive()
  validUsername: (name) ->
    @usernameDisabled() or /^\w{3,12}$/.test(name)
  style:
    btn3:
      'margin-top': '15px'
  signal: 'keyboard'
  autorun: [
    ->
      console.log "TMP:editTeacher:autorun: testData = ", @isTestData()
    ->
      key = @keyDown().which
      @editMode false if key is 27
    ]
  cancel: ->
    @editMode false
  onRendered: (t) ->
    ## test
    t.subscribe 'main', =>
      @_id('qtD9cj4GgsfNEb9Tp')
      Session.set "admin_file", 'Volunteers'
      console.log "TMP:editTeacher:onCreated *1* ID, Noun, testData = ", @_id(), @noun(), @isTestData()

      @load @table().findOne(@_id()) if @_id()
      console.log "TMP:editTeacher:onCreated *2* testData, name = ", @isTestData(), @name()
      if @_id() and @nounIs 'Users'
        @load @profile?()
        @email @emails()[0].address
        @validUser @_id() and @name() and @number() and @email() and @username() and @userType()
      console.log "TMP:editTeacher:onCreated *3* testData = ", @isTestData()
      return
  updateText: ->
    if @_id() then "Update Information" else "Create"
  canUpdate: ->
    @name() and @validNumber(@number()) and @validEmail(@email()) and @validUsername(@username())
  hasProfile: ->
    @active() and @noun() isnt "Users" or @userType()
  events:
    'submit form': (e) ->
      e.preventDefault()
      @update()
  update: ->
    return if not @canUpdate()
    data =
      name: @name()
      number: @number()
      email: @email()
      notActive: @notActive()
      isTestData: @isTestData()
    if @_id()
      Meteor.call 'contacts.update', @table()._name, @_id(), data, (err) =>
        if err
          Bert.alert "Could not update:<br>" + err, "danger"
        else
          Bert.alert "Updated", "success"
          @editMode false
        return
    else
      Meteor.call 'contacts.insert', @table()._name, data, (err, id) =>
        if err
          Bert.alert "Could not create:<br>" + err.reason, "danger"
        else
          Bert.alert "Created", "success"
          @_id(id)
        return
  profileClick: ->
    if @canUpdate()
      id = @_id()
      page = 'profileTeacher'
      switch @noun()
        when 'Users'
          switch @userType()
            when "V"
              profile = Volunteers.findOne
                userId: id
              page = 'profileVolunteer'
            when "T"
              profile = Teachers.findOne
                userId: id
          id = profile?._id
        when 'Volunteers'
          page = 'profileVolunteer'
      if page and id
        Router.go page, id:id
