import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.editTeacher.viewmodel
  mixin: [
    'email'
    'table'
    'user'
  ]
  share: [
    'edited'
  ]
  id: ''  #passed
#  item: {}
  profile: ''
  usernameDisabled: true
  validUser: true
  hasChanged: false

  validUsername: (name) ->
    @usernameDisabled() or /^\w{3,12}$/.test(name)
  getId: ->
    console.log "TMP:editTeacher:getId: id, Router.id", @id.value, Router.getParam 'id'
    @_id @id() or Router.getParam('id')
  style:
    btn3:
      'margin-top': '15px'
  signal: 'keyboard'
  autorun: [
    ->
      key = @keyDown().which
      @editMode false if key is 27
    ->
      if @templateInstance.subscriptionsReady()
        @item = @table().findOne @getId()
        @load @item
        console.log "TMP:editTeacher:autorun: ITEM loaded"
        if @item and @nounIs 'Users'
          @load @item.profile
          @email @item.emails[0].address
          @validUser @_id.value and @name.value and @number.value and @email.value and @username.value and @userType.value
        unless @nounIs 'Users'
          @validUser @_id.value
      else
        console.log "TMP:editTeacher:A sub NOT ready"
  ]
  cancel: ->
    @editMode false
  onCreated: (tmpl) ->
    tmpl.subscribe 'all'
  updateText: ->
    if @_id() then "Update Information" else "Create"
  canUpdate: ->
    @name() and @validNumber(@number()) and @validEmail(@email()) and @validUsername(@username())
  allowInactive: ->
    @.id() and @._id() isnt Meteor.userId() and (@username() isnt 'admin' or @noun() isnt 'Users')
  hasProfile: ->
    @isActive() and (@noun() isnt "Users" or @userType())
  events:
    'submit form': (e) ->
      e.preventDefault()
      @update()
  update: ->
    return if not @canUpdate()
    data =
      name: @name()
      number: @number()
      email: @email()
      isActive: @isActive()
      isTestData: @isTestData()
    data.userId = @userId() if @userId()
    if @_id()
      wasActive = @item.isActive
      Meteor.call 'contacts.update', @table()._name, @_id(), data, (err) =>
        if err
          Bert.alert "Could not update:<br>" + err, "danger"
        else
          Bert.alert "Updated", "success"
          if @table() isnt 'Users'
            console.log "TMP:editTeacher:update: userId, isActive, wasActive, data=",data.userId, data.isActive,wasActive,data
            if data.userId and not data.isActive and wasActive
              console.log "TMP:editTeacher:update: set NOT ACTIVE.  data=",data
              Meteor.call 'users.clearType', data.userId
          @editMode false
        return
    else
      Meteor.call 'contacts.insert', @table()._name, data, (err, id) =>
        if err
          Bert.alert "Could not create:<br>" + err.reason, "danger"
        else
          Bert.alert "Created", "success"
          @_id(id)
          @validUser yes
        return
  profileClick: ->
    if @canUpdate()
      id = @_id()
      page = 'profileTeacher'
      switch @noun()
        when 'Users'
          switch @userType()
            when "V"
              profile = Volunteers.findOne
                userId: id
              page = 'profileVolunteer'
            when "T"
              profile = Teachers.findOne
                userId: id
          id = profile?._id
        when 'Volunteers'
          page = 'profileVolunteer'
      if page and id
        Router.go page, id:id
