#noinspection JSUnresolvedVariable
Template.prefCard.viewmodel
  share:[
    'edited'
    'user'
    'thisTeacher':'thisTeacher'
  ]
  validUser: false
  autorun: ->
    @validUser @_id() and @name() and @number() and @email() and @username()
  save: ->
    return unless @newType()
    data =
      userType: @newType()
    id = @_id()
    Meteor.call 'users.update', {id, data}, (err) =>
      if err
        Bert.alert "Could not update:<br>" + err, "danger"
      else
        Bert.alert "Set user type", "success"
        @addItem(id, data)
  addItem: (id, data) ->
    check data, Object
    check id, String
    delete data.userType
    data.userId = id
    data =
      name: @name()
      number: @number()
      email: @email()
      isActive: true
      isTestData: @isTestData() if @isTestData
      userId: id
      username: @username()
    tablename = if @newType() is "T" then "Teachers" else "Volunteers"
    Meteor.call 'contacts.upsert', tablename, id, data, (err) =>
      if err
        Bert.alert "Could not create new record in %s:<br>",tablename + err.reason, "danger"
      else
#        Bert.alert "Created in %s", tablename, "success"
        @userType @newType()
    return


