Template.admin_menu.viewmodel
  share: 'search'
  style:
    backgroundColor: 'royalblue'
  buttons: [
    "Users"
    "Teachers"
    "Volunteers"
    "Tasks"
  ]

  signal: 'keyboard'
  autorun: ->
    key = @keyDown().which
    @searchText "" if key is 27
  onRendered: ->
    Session.setDefault "admin_file", "Teachers"
  select: (word) ->
    Session.set "admin_file", word
    if model = ViewModel.findOne('contacts')
      model.sid ''
      model.editMode false
  active: (word) ->
    word is Session.get "admin_file"
