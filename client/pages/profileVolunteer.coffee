import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.profileVolunteer.viewmodel
  share: [
    'edited'
    'user'
  ]
  editMode: true
  hasChanged: false
  userId: ""

  isActive: true
  isTestData: false

  signal: 'keyboard'
  autorun: [
    ->
      key = @keyDown().which
      @editMode false if key is 27
    ->
      history.back() unless @editMode()

    ]
  cancel: ->
    @editMode false
  onRendered: ->
    @_id Router.getParam('id') or Meteor.userId()
    Meteor.subscribe 'one', @_id(),  =>
      @load Volunteers.findOne @_id()
      Meteor.setTimeout =>
        @hasChanged(false)
      ,200
  events:
    'submit form': (e) ->
      e.preventDefault()
      @update()
  canUpdate: ->
    @hasChanged()
  update: ->
    return if not @canUpdate()
    data = _.pick @data(), 'address city state zip'.split(' ')
    if @_id()
      Meteor.call 'contacts.update', 'Volunteers', @_id(), data, (err) =>
        if err
          Bert.alert "Could not update:<br>" + err, "danger"
        else
          Bert.alert "Updated", "success"
#          history.back()
          @editMode(false)
        return
