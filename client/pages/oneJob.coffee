import { isAdmin } from '/imports/library/contextUtilities'
import {Router} from '/imports/library/default'
#noinspection JSUnresolvedVariable
Template.oneJob.viewmodel
  mixin: [
    'job'
  ]
#  xTask: ->
#    Tasks.findOne @taskId()
  rowHover: false
  taskCode: ->
    @xTask().code
  rowClick: ->
    console.log "TMP:oneJob: rowClick"
    Router.go 'job1', id:@_id()
  deleteTitle: ->
    'Job \'' + @jobName() + '\' created ' + @createdAt?()
  delete: (e) ->
    e.stopPropagation()
    #noinspection JSUnresolvedVariable
    Client.alert
      header: 'Are you sure you want to delete \'' + @jobName() + '\'?'
      description: 'You\'re about to delete ' + @deleteTitle() + '. Do you really want to delete it?'
      image: 'trash'
      onApprove: =>
        console.log "Would delete %s here", @_id()
#        Jobs.remove @_id()
  isAdmin: -> isAdmin()
