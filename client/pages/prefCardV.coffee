import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.prefCardV.viewmodel
  share: 'thisTeacher'
  id: ""  # user ID
  _id: ""  # Volunteer ID
  isActive: true
  name: "unknown"
  canUpdate: true
  fields: [
    'address'
    'city'
    'state'
    'zip,ZIP'
  ]
  address: ''
  city: ''
  state: ''
  zip: ''
  field: (name) ->
    @[name.split(',')[0]]?()
  title: (name) ->
    [field, title] = name.split ',',2
    title or field.substr(0,1).toUpperCase() + field.substr(1)

  onRendered: ->
    @reset()
    @id Router.getParam('id') or Meteor.userId()
    Meteor.subscribe 'me',  =>
      data = Volunteers.findOne
        userId: @id()
      if data?._id
        @load data
        @volunteerId @_id()
      else
        Meteor.call 'users.clearType', @id()
        Meteor.user().profile.userType = ''
  edit: ->
    Router.go 'profileVolunteer', id:@_id() if @canUpdate()

