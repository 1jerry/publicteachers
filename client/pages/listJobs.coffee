import {Router} from '/imports/library/default'

Template.listJobs.viewmodel
  share: ['edited', 'thisTeacher']

  onRendered: ->
    Meteor.subscribe('all')
  validUser: ->
    @teacherId() or @volunteerId()
  jobs: ->
    find = {teacherId:@teacherId?()}
    console.log "TMP:listJobs:jobs: find:", find
    Jobs.find find, sort: createdAt: -1
  rowClick: ->
    Router.go 'job'
