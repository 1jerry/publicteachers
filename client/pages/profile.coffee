import { FlowRouter } from 'meteor/ostrio:flow-router-extra'

person =
  cat: ''
  retired: ''
  name: ViewModel.property.string.notBlank.min 4
  email: ''
  username: ''
  certificate: ''
  schoolDistrict: ''
  schoolName: ''
  schoolGrade: ''
  boxes: {}
  roles: []
  contextText: ->
    if @_id then "Update Information" else "Create Contact"
  isTeacher: ->
    @cat() is "i1"
  isVolunteer: ->
    @cat() is "i2" or @cat() is 'i3'
  isRetired: ->
    @cat() is "i2" and @retired()
  wasTeacher: ->
    @cat() is "i2"
  nonTeacher: ->
    @cat() is "i3"
  checkLog: ->
    console.log "click.  catSelected = ",@cat()
  certActive: ->
    !!@boxes()["certActive"]


ViewModel.share
  person: person
ViewModel.mixin
  person: person

#noinspection JSUnresolvedVariable
Template.profile1.viewmodel
  share: 'person'
  firstTime: false
  onRendered: ->
    @reset()
    Meteor.subscribe('main')
  autorun: ->
    id = FlowRouter.getParam('id')
    data = ''
    data = Meteor.users.findOne(id) if !!id
    console.log "In signup template, id= #{id}, found = #{!!data}, #{data?._id}" if dbg.dbg
    data or= Meteor.user() if Meteor.userId()
    console.log "before load: id= #{data?._id}" if dbg.dbg
    @load( data)
    console.log "after load, viewmodel id = #{@_id()}" if dbg.dbg
    @load( data.profile) if data.profile?
    dbg.profile = @
    @email( @emails()[0].address) if data.emails?
    return
  emailChanged: ->
    @email() != @emails()[0].address
  updateText: ->
    if not @firstTime() then "Update Information" else "Create Profile"
  canUpdate: ->
    true
  cancel: ->
    history.back()
  update: ->
    return unless @canUpdate()
    contact =
      cat: @cat()
      retired: @retired()
      name: @name()
      schoolDistrict: @schoolDistrict()
      schoolName: @schoolName()
      schoolGrade: @schoolGrade()
      certificate: @certificate()
      boxes: @boxes()
#    contact.emails[0].address = @email()
    updateRecord = [
      profile: contact,
    ]
    console.log "email changed?", @emailChanged()
    updateRecord.push "emails.$.address": @email() if @emailChanged()
    Meteor.call 'users.update',
      id:@_id()
      data:contact
    , (err) ->
      if err
        Bert.alert  "NOT Updated - " + err.reason, "danger"
      else
        history.back()
        Bert.alert "Updated", "success"
      return
