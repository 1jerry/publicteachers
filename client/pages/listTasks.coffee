Template.listTasks.viewmodel
  share: 'search'
  mixin: 'table'

  onRendered: ->
    Meteor.subscribe('main')
    @showSearch true
    return
  onDestroyed: ->
    @showSearch false
    return
  searchObject: ->
    searchObject = {}
    if @searchText()
      r = new RegExp('.*' + @searchText() + '.*', 'i')
      searchObject['$or'] = [
        { name: r }
        { code: r }
      ]
    searchObject
  tasks: ->
    find = @searchObject()
    Tasks.find find, sort: code: 1

# ---
