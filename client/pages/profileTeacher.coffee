import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.profileTeacher.viewmodel
  share: [
    'edited'
    'user'
  ]
  editMode: true
  hasChanged: false
  userId: ""

  nameColor: 'blue'
  signal: 'keyboard'
  autorun: [
    ->
      key = @keyDown().which
      @editMode false if key is 27
    ->
      history.back() unless @editMode()

    ]
  cancel: ->
    @editMode false
  onRendered: ->
    @_id Router.getParam('id') or Meteor.userId()
#    console.log "TMP:profileTeacher:onRendered: _id=",@_id()
    Meteor.subscribe 'one', @_id(),  =>
#      console.log "TMP:profileTeacher:onRendered:load teacher: _id=",@_id()
      @load Teachers.findOne @_id()
      Meteor.setTimeout =>
        @hasChanged(false)
      ,200
  events:
    'submit form': (e) ->
      e.preventDefault()
      @update()
  canUpdate: ->
    @hasChanged()
  update: ->
    return if not @canUpdate()
    data = _.pick @data(), 'userId YOS district school grade certNo'.split(' ')
    if @_id()
      Meteor.call 'contacts.update', 'Teachers', @_id(), data, (err) =>
        if err
          Bert.alert "Could not update:<br>" + err, "danger"
        else
          Bert.alert "Updated", "success"
#          history.back()
          @editMode(false)
        return
