import { isAdmin } from '/imports/library/contextUtilities'
#noinspection JSUnresolvedVariable
Template.oneTask.viewmodel
  share: [
    edited: 'edited'
  ]
  mixin: [
    'table'
    'user'
  ]
  rowHover: false
  rowClick: ->
    @edited.sid @_id()
    @edited.editMode true
  deleteTitle: ->
    'Delete \'' + @name() + '\' from ' + @noun()
  delete: (e) ->
    e.stopPropagation()
    #noinspection JSUnresolvedVariable
    Client.alert
      header: 'Are you sure you want to delete \'' + @name() + '\'?'
      description: 'You\'re about to delete ' + @deleteTitle() + '. Do you really want to delete it?'
      image: 'trash'
      onApprove: =>
        Tasks.remove @_id()

