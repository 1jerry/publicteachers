import { isAdmin } from '/imports/library/contextUtilities'
import {Router} from '/imports/library/default'
#noinspection JSUnresolvedVariable
Template.oneVolunteer.viewmodel
  rowHover: false
  rowClick: ->
    Bert.alert "row clicked for " + @name()
  isAdmin: -> isAdmin()
