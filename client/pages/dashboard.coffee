import "./dashboard.styl"
import { FlowRouter } from 'meteor/ostrio:flow-router-extra';

#noinspection JSUnresolvedVariable
Template.dashboard.viewmodel
  share:[
    'edited'
    'user'
  ]
  mixin:[
  ]
  ready: ->
    @templateInstance.subscriptionsReady()  # doesn't seem to work
  autorun: [
    ->
      @reset()
      if item = Meteor.user()
        @load item
        @load item.profile
        @email item.emails[0].address
        console.warn "TMP:dashboard:autorun.LOAD, id, name =",@_id.value, @name.value
  ]
  onRendered: ->
    Meteor.subscribe('main')
  edit1: ->
    FlowRouter.go 'editMe'
  edit2: ->
    FlowRouter.go 'profile'
