import {Router} from '/imports/library/default'

#noinspection JSUnresolvedVariable
Template.prefCardT.viewmodel
  share: 'thisTeacher'
  id: ""  # user ID
  _id: ""  # Teacher ID
  isActive: true
  name: "unknown"
  canUpdate: true
  fields: [
    'YOS,First Year'
    'district'
    'school'
    'grade'
    'certNo'
  ]
  YOS: ''
  district: ''
  school: ''
  grade: ''
  certNo: ''
  field: (name) ->
    @[name.split(',')[0]]?()
  title: (name) ->
    [field, title] = name.split ',',2
    title or field.substr(0,1).toUpperCase() + field.substr(1)

  onRendered: ->
    @reset()
    @id Router.getParam('id') or Meteor.userId()
    Meteor.subscribe 'me',   =>
      data = Teachers.findOne
        userId: @id()
      if data?._id
        @load data
        @teacherId @_id()
      else
        Meteor.call 'users.clearType', @id()
        Meteor.user().profile.userType = ''
  edit: ->
    Router.go 'profileTeacher', id:@_id() if @canUpdate()

