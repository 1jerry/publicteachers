import { isAdmin } from '/imports/library/contextUtilities'
#noinspection JSUnresolvedVariable
Template.oneTeacher.viewmodel
  share: [
    edited: 'edited'
  ]
  mixin: [
    'table'
    'user'
  ]
  rowHover: false
  onRendered: ->
#    console.log "TMP:oneTeacher:onRendered:<<<",@data()
    if @noun() is 'Users'
      @load( @profile?())
      @email( @emails()[0].address)
#    console.log "TMP:oneTeacher:onRendered:>>>",@data()
  rowClick: ->
    @edited.sid @_id()
    @edited.editMode true
  deleteTitle: ->
    'Delete \'' + @name() + '\' from ' + @noun()
  thumbnail: ->
    @table().findOne(@_id())?.image?() or Global.defaultImage
  delete: (e) ->
    e.stopPropagation()
    #noinspection JSUnresolvedVariable
    Client.alert
      header: 'Are you sure you want to delete \'' + @name() + '\'?'
      description: 'You\'re about to delete ' + @deleteTitle() + '. Do you really want to delete it?'
      image: 'trash'
      onApprove: =>
        if @nounIs('Users')
          Meteor.call 'users.delete', @_id(), (err) =>
            if err
              Bert.alert "Could not remove:<br>" + err, "danger"
            else
              Bert.alert "Removed", "success"
        else
          @table().remove @_id()
  isAdmin: ->
    if @noun() is 'Users' and isAdmin @_id() then 'admin'

