Template.listUsers.viewmodel
  share: [
    'search'
  ]
  mixin: 'table'

  onRendered: ->
    Meteor.subscribe('main')
    @showSearch true
    return
  onDestroyed: ->
    @showSearch false
    return
  searchObject: ->
    searchObject = {}
    if @searchText()
      r = new RegExp('.*' + @searchText() + '.*', 'i')
      searchObject['$or'] = [
        { username: r }
        { 'emails.address': r }
        { 'profile.name': r }
      ]
    searchObject
  teachers: ->
    find = @searchObject()
    @table().find find, sort: username: 1

# ---
