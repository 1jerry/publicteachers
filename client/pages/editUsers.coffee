#noinspection JSUnresolvedVariable
Template.editUsers.viewmodel
  _id: ''
  share:[
    'edited'
  ]
  onRendered: ->
#    Meteor.subscribe('main')
    Session.set "admin_file", 'Users'
    this._id(Meteor.userId())
    @editMode true
  autorun: ->
    history.back() unless @editMode()
