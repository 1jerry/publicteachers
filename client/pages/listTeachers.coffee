Template.listTeachers.viewmodel
  share: 'search'
  mixin: 'table'

  onRendered: ->
    Meteor.subscribe('main')
    @showSearch true
    return
  onDestroyed: ->
    @showSearch false
    return
  searchObject: ->
    searchObject = {}
    if @searchText()
      r = new RegExp('.*' + @searchText() + '.*', 'i')
      searchObject['$or'] = [
        { name: r }
        { email: r }
      ]
    searchObject
  teachers: ->
    find = @searchObject()
    @table().find find, sort: name: 1

# ---
