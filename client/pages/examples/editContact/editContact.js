
Template.editContact.viewmodel({
  onRendered: function() {
    //noinspection JSUnresolvedFunction
    this.load( Meteor.user() );
    this.load( Meteor.user().profile );
    // this.email( "2015@jerry.com")
  },
  _id: '',
  name: '',
  number: '',
  email: '',
  validEmail: function (email) {
    return (email.indexOf("@") > 1)
  },
  upsertText: function () {
    return this._id() ? "Update Information" : "Create Contact";
  },
  canUpsert: function () {
    return this.name() && this.number() && this.validEmail(this.email()) ;
  },
  upsert: function () {
    let self = this;
    var Contacts = Meteor.users;
    const userId = Meteor.userId();
    if (!self.canUpsert()) {
      return;
    }
    var contact = {
      name: this.name(),
      number: this.number(),
      email: this.email()
    };

    if (self._id()) {
      Contacts.update(userId, {$set: {profile:contact}}, function (err) {
        if (err) {
          Bert.alert("Could not update contact:<br>" + err.reason, "danger")
        } else {
          Bert.alert("Profile updated","success")
        }
      });
    }
  },
  "": function () {
    // to satisfied function names in WebStorm
    return self.upsertText() && self.upsert() && self.validEmail();
  }

});
Template.emailtest.viewmodel({
  value: '',
  name: '',
  fld: '',
  onRendered: function() {
    dbg.model = this;
    // this.value( this.parent()[this.name()]());
    // this.value.changed();
    console.log('name',this.name(),' value',this.value())
  },
  autorun: function () {
    this.parent()[this.name()](this.fld())
  }
});

Template.formTest.viewmodel({
  name: '',
  color: '',
  city: '',
  onRendered: function () {
    this.load( {name:"Bob", city:"Fargo", color:"Muted"});  // Simulate record read
  },
  byRef(prop) {
    return () => this[prop]
  }
});
Template.fieldTest.viewmodel({
  // the only generic field template that works.
  // requires passing value AND name, because there is no way of getting the name from the actual "function property"
  autorun: function () {
    this.parent()[this.name()](this.fld())
  }
});
// fieldTest1 - do nothing
Template.fieldTest1.viewmodel({
  fld: ''
});
Template.fieldTest2.viewmodel({
  value: '',
  onRendered: function() {
    this.value =  this.parent()[this.name()];
    this.value(this.fld())
  }
});
Template.fieldTest3.viewmodel({
  text: ''
});

