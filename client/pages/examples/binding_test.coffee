Template.test_question.viewmodel
  text: ''
  required: false
  valid: ->
    !@required() or @text()

Template.binding_test.viewmodel
  fullName: ->
    @firstName?.text() + ' ' + @lastName?.text() if @firstName?.text? and @lastName.text?
  logName: ->
    alert @firstName.text() + ' ' + @lastName.text()
