// noinspection JSUnusedGlobalSymbols
Template.card.viewmodel({
  share: ['edited'],
  // mixin: 'user',
  _id: '',
  hasTags: function () {
    return this.parent().roles() !== '' || this.parent().userType() !== ''
  },
  imageUrl: function(){
    var contact = this._id && Teachers.findOne(this._id() || "");
    return contact && contact.image() || Global.defaultImage;
  },
  roles: function () {
    return this.parent().roles()
  },
  isUser: function () {
    return Boolean(this.parent().profile && this.parent().profile()) || this.parent().userId()
  }
});
