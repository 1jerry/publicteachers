import { Migrations } from 'meteor/percolate:migrations'

Migrations.add
  version: 1
  name: "Initial migration - add roles"
  up: ->
    if Meteor.users.find().count()
      uo = Meteor.users.find({username:{$in:['admin','cindy']}})
      _.each(uo.fetch(), (user) ->
        console.log "Setting '#{user.username}' to admin"
        Roles.addUsersToRoles user._id, ['admin']
        return)
