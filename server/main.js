// Methods
import "/imports/api/methods";
import "/imports/api/methods/server";

// Collections
import "/imports/api/collections";
import "/imports/api/collections/helpers";
import "/imports/api/collections/server";

// Server setup
import "/imports/startup/server.js"
