Meteor.methods
  "users.clearType": (id) ->
    # clear userType on this user when inActive
    Meteor.users.update id, $set: {'profile.userType':''} if id

  "users.delete": (id) ->
    return if Meteor.isClient
    item = Meteor.users.findOne id
    if not item
      MError 'Not Allowed'
    error = ''
    profile = item.profile
    if profile.name
      if profile.userType
        error = 'Active Teacher or Volunteer'
      else if profile.isActive
        error = 'Active user.  Set to inactive to delete'
    if error
      MError error
    else
      Meteor.users.remove id

  "users.update": ({id,data}) ->
    delete data.email  # for now
    user = Meteor.users.findOne(id)
    MError "Attempt to update non-existent user id '%s'", id if not user
#    _.mapKeys(pf, function(v,k) {return 'profile.'+k})  # alternate for set object
    newProfile = user.profile or {}
    Object.assign(newProfile, data)
    Meteor.users.update id, $set: {profile:newProfile}
    name = if newProfile.userType is 'T' then 'Teachers' else 'Volunteers'
    global[name].update
      userId: id
    ,
      $set:
        _.pick newProfile, [
          'username'
          'name'
          'number'
        ]

  "contacts.insert": (table, data) ->
    check table, String
    check data, Object
    collection = switch
      when table is "Teachers" then Teachers
      when table is "Volunteers" then Volunteers
      else MError 'Invalid Collection: %s', table
    data.createdAt = Date.now()
    data.createdBy = @userId
    _.defaults data,
      username: ''
    collection.insert data,
      (error) ->
        MError error if error

  "contacts.update": (table, id, data) ->
    check table, String
    check table, Match.OneOf 'Teachers', 'Volunteers', 'users'  # lower-case 'users' here
    check id, String
    check data, Object
    return Meteor.call 'users.update', {id, data} if table is "users"
    collection = switch
      when table is "Teachers" then Teachers
      when table is "Volunteers" then Volunteers
      else MError 'Invalid Collection: %s', table
    collection.update id, $set: data, (error) ->
      MError error if error
    if data.userId and 'name' of data
      Meteor.users.update data.userId, $set: {'profile.name':data.name}
    if data.userId and 'number' of data
      Meteor.users.update data.userId, $set: {'profile.number':data.number}

  "contacts.upsert": (table, id, data) ->
    check table, String
    check data, Object
    collection = switch
      when table is "Teachers" then Teachers
      when table is "Volunteers" then Volunteers
      else MError 'Invalid Collection: %s', table
    if not collection.findOne({userId:id})
      data.createdAt = Date.now()
      data.createdBy = id
    _.defaults data,
      username: ''
    collection.update {userId:id}, $set:data,
      upsert:true
      (error) ->
        MError error if error

  "jobs.update": (id, data) ->
    # update / add general information
    check id, Match.Maybe String
    check data, Object
    check data.teacherId, String
    Jobs.update(id, $set:data,
      upsert:true
      (error) ->
        MError error if error
    )
  "jobs.fetch": (forId) ->
    check forId, match.Maybe String
    searchObj = {}
    searchObj.teacherId = forId if forId
    Jobs.find searchObj
      .fetch()

  "tasks.insert": ( data) ->
    check data, Object
    data.createdAt = Date.now()
    data.createdBy = @userId
    data.addedBy = Meteor.user().username
    _.defaults data,
    Tasks.insert data,
      (error) ->
        MError error if error

  "tasks.update": (id, data) ->
    check id, String
    check data, Object
    Tasks.update id, $set: data, (error) ->
      MError error if error
