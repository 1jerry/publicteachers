import Triggers from "./triggers"
import { FlowRouter } from 'meteor/ostrio:flow-router-extra'
import { Tracker } from 'meteor/tracker'

dbg.dbg = false

updateView = (templateName, options = {}) ->
  Meta.setTitle FlowRouter.getRouteName()
  layoutName = options.layout or 'mainLayout'
  FlowRouter.Renderer.render layoutName, templateName, options

Tracker.autorun ->
  # reset page if logged in or out
  user = Meteor.user()
  userId = Meteor.userId()
#  console.log('Routes: userid ',userId) if dbg.dbg
  return unless Meteor.status().connected
  return if userId and not user
  route = FlowRouter.current()?.route
  if route?.group
    console.log('Routes: group %s, route %s',route.group, route.group?.name) if dbg.dbg
    if user
      FlowRouter.reload() if route.group.name is 'public'
    else
      FlowRouter.reload() if route.group.name in ['private', 'admin']

###
# Types of routes
###
Routes = FlowRouter.group(
  name: 'allRoutes'
  whileWaiting: ->
    @render 'loading'
  triggersEnter: [
    Triggers.noInfLoad
    Triggers.mustNotBe.blocked
  ])
allRoutes = Routes

publicRoutes = Routes.group(
#  a route that is NOT allowed if you are logged in
  name: 'public'
  triggersEnter: [
    Triggers.mustNotBe.loggedIn
  ]
)

privateRoutes = Routes.group(
  name: 'private'
  triggersEnter: [
    Triggers.mustBe.loggedIn
  ]
)

adminRoutes = privateRoutes.group(
  prefix: '/admin'
  name: 'admin'
  triggersEnter: Triggers.mustBe.admin
)

###
Actual routes
###

allRoutes.route '/',
  name: 'public_home'
  action: ->
    updateView 'home'
allRoutes.route '/hug-a-book',
  name: 'hug'
  action: ->
    updateView 'not_yet'
publicRoutes.route '/login',
  name: 'login'
  action: ->
    updateView 'loginPage'
allRoutes.route '/training',
  name: 'train'
  action: ->
    updateView 'not_yet'

################
privateRoutes.route '/home',
  name: 'dashboard'
  action: ->
    updateView 'dashboard'
privateRoutes.route '/changes',
  name: 'changes'
  action: ->
    updateView 'change_log'
privateRoutes.route '/profile/:id',
  name: 'profile'
  action: ->
    updateView 'profile'
privateRoutes.route '/profile',
  name: 'profileDefault'
  action: ->
    updateView 'profile'
privateRoutes.route '/profileT/:id',
  name: 'profileTeacher'
  action: ->
    updateView 'profileTeacher'
privateRoutes.route '/profileV/:id',
  name: 'profileVolunteer'
  action: ->
    updateView 'profileVolunteer'
privateRoutes.route '/signup/:id',
  name: 'signup1'
  action: ->
    updateView 'signup'
privateRoutes.route '/signup',
  name: 'signup'
  action: ->
    updateView 'signup'
privateRoutes.route '/edit/:id',
  name: 'editOne'
  action: ->
    updateView 'editUsers'
privateRoutes.route '/edit',
  name: 'editMe'
  action: ->
    updateView 'editUsers'
privateRoutes.route '/examples',
  name: 'examples'
  action: ->
    updateView 'examples'
privateRoutes.route '/job/:id',
  name: 'job1'
  action: ->
    updateView 'job'
privateRoutes.route '/job',
  name: 'job'
  action: ->
    updateView 'job'

###############
adminRoutes.route '/',
  name: 'admin'
  action: ->
    updateView 'admin'
adminRoutes.route '/test',
  name: 'test'
  action: ->
    updateView 'test'

###############
FlowRouter.route '*',
  name: 'notFound'
  action: ->
    Meta.setTitle '404: Page not found'
    console.log '[ROUTER] ::: 404 - route not found'
    @render 'mainLayout', 'notFound'
