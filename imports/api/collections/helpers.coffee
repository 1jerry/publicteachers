image = ->
  if @imageFile
    '/upload/' + @imageFile + '?v=' + @imageVersion
  else
    Global.defaultImage
username = ->
  if @userId then Meteor.users().findOne(@userId).username else ""

Teachers.helpers
  image: image
#  username: username
Volunteers.helpers
#  username: username
Jobs.helpers
  jobOwner: ->
    id = if typeof @teacherId == 'function' then @teacherId() else @teacherId  #prevent crash in VM
    data = Teachers.findOne id
    if data then data.name else 'not yet set'
#  xTask: ->
#    fld = @taskId
#    id = if typeof fld == 'function' then fld() else fld  #prevent crash in VM
#    Tasks.findOne id
#export {Teachers, Volunteers}
