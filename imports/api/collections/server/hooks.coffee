export hooks =
  before:
    insert: (userId, doc) ->
      doc.createdAt = Date.now()
      doc.createdBy = userId
      return
    update: (userId, doc, fieldNames, modifier, options) ->
      modifier.$set or= {}
      modifier.$set.modifiedAt = Date.now()
      modifier.$set.modifiedBy = userId


