Meteor.publish 'main', ->
  if Roles.userIsInRole @userId, 'admin'
    [
      Meteor.users.find()
      Teachers.find()
      Volunteers.find()
      Jobs.find()
      Tasks.find()
    ]
Meteor.publish 'all', ->
  [
    Meteor.users.find()
    Teachers.find()
    Volunteers.find()
    Jobs.find()
    Tasks.find()
  ]
Meteor.publish 'volunteers', ->
  Volunteers.find()
Meteor.publish 'me', (id) ->
  check id, Match.Maybe String
  id = @userId unless id
#  console.log "TMP:publish:'me': id='%s'", id
  [
    Teachers.find
      userId: id
    Volunteers.find
      userId: id
    Jobs.find
      createdBy: id
  ]
Meteor.publish 'one', (id) ->
  check id, String
  [
    Teachers.find id
    Volunteers.find id
    Jobs.find id
  ]

Meteor.publish 'tasks', ->
  Tasks.find()
