Tasks.allow
  insert: -> true
  update: -> true
  remove: -> true

import {hooks} from './hooks'
Tasks.before.insert(hooks.before.insert)
Tasks.before.update(hooks.before.update)
