Jobs.allow
  insert: -> true
  update: -> true
  remove: -> true

import {hooks} from './hooks'
Jobs.before.insert(hooks.before.insert)
Jobs.before.update(hooks.before.update)
