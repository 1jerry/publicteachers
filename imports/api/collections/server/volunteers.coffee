Volunteers.allow
  insert: -> true
  update: -> true
  remove: -> true

import {hooks} from './hooks'
Volunteers.before.insert(hooks.before.insert)
Volunteers.before.update(hooks.before.update)
