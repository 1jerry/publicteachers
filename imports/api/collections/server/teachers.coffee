Teachers.allow
  insert: -> true
  update: -> true
  remove: -> true

import {hooks} from './hooks'
Teachers.before.insert(hooks.before.insert)
Teachers.before.update(hooks.before.update)
