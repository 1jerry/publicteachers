export isAdmin = (user=null) => Roles.userIsInRole user or Meteor.user(), ['admin']
