## export specific library as generic names

import { FlowRouter } from 'meteor/ostrio:flow-router-extra'
#noinspection JSUnresolvedVariable
export Router = FlowRouter
