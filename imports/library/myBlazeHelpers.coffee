export formatDate = (context = new Date(), option="DT") ->
  dfmt = "MM/DD/YYYY"
  tfmt = "HH:mm:ss"
  m = moment context
  if m._isValid
    switch
      when option is 'ago' then m.fromNow()
      when option is 'cal' then m.calendar()
      when option is "D" then m.format dfmt
      when option is "T" then m.format tfmt
      when option is "DT" then m.format dfmt + ', ' + tfmt
      when option is "TD" then m.format tfmt + 'on ' + dfmt

dbg.formatDate = formatDate

Template.registerHelper "formatDate", formatDate
