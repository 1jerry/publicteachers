###
  constants
###
emailRegex = /^([\w-+]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

###
  layouts
###
job =
  _id: ''
  jobName: ViewModel.property.string.notBlank.min 4
  jobDescription: ViewModel.property.string
  jobOwner: 'you'  # will be redefined
  taskId: ''
  teacherId: ''
  volunteers: []
  createdAt: ''
  createdBy: ''
  modifiedAt: ''
  modifiedBy: ''
  isActive: true
  isTestData: false
  xTask: ->
    Tasks.findOne @taskId()

task =
  _id: ''
  code: ViewModel.property.string.notBlank.min 2
  name: ViewModel.property.string.notBlank.min 4
  description: ViewModel.property.string
  addedBy: ''
  requireTeacher: false
  requireOnsite: true
  estTime: ''
  createdAt: ''
  createdBy: ''
  modifiedAt: ''
  modifiedBy: ''
  isActive: true
  isTestData: false
user =
  _id: ''
  name: ''
  number: ''
  email: ''
  username: ''
  userType: ''  # only Users
  isActive: true
  isTestData: false
  roles: []  # only Users
  userId: ''  # not Users
  isTeacher: -> @userType() is "T"
  isVolunteer: -> @userType() is "V"

###
  Mixins
###
ViewModel.mixin
  job:job
  user:user
  task:task
  email:
    validEmail: (email) ->
      !!email && emailRegex.test(email)
    validNumber: (number) ->
      not isNaN(number) and number.length in [7,10]
  table:
    amAdmin: ->
      Meteor.user().username is 'admin'
    noun: ->
      Session.get "admin_file"
    nounIs: (word) ->
      word is @noun()
    table: ->
      switch
        when @noun() is "Volunteers" then Volunteers
        when @noun() is "Users" then Meteor.users
        else Teachers

###
  Shares
###
ViewModel.share
  user:user
  thisTeacher:
    teacherId: ''
    volunteerId: ''
  edited:
    sid: ''
    editMode: false
  search:
    searchText: ''
    showSearch: false

###
  Signals
###
ViewModel.signal
  keyboard:
    keyDown:
      target: document
      event: 'keydown'

###
  debugging utility
###
dbg.vmList = ->
  _.forEach ViewModel.find(), (vm) ->
    if vm.templateName()
      console.log vm.templateName(), '"'+(vm.name?() or "")+'"', vm.vmId, "("+(vm.parent()?.vmId or "")+")", vm.vmPathToParent()
