initialTasks = [
  ['mt','Mentor a teacher',1]
  ['ac','Adopt a classroom']
  ['ah','Help with assessments',1]
  ['rah','Help set up and maintain a Read at Home program',1]
  ['tsl','Tutor individual student in Literacy']
  ['tsm','Tutor individual student in Math']
  ['asi','Implement after-school enrichment club']
  ['lb','Be a once-a-week lunch buddy']
  ['ss','Be a speaker on a specific subject']
  ['tsa','Teach a special activity']
  ['rd','Help with room decor and/or materials for class instruction']
  ['te','Teach English to interested parents']
  ['pw','Give parent workshops to interested parents']
]

loadTasks = (line) ->
  exists = !!Tasks.findOne(
    code: line[0]
  )
  if not exists
    task =
      code: line[0]
      name: line[1]
      requireTeacher: line[2] or false
      requireOnsite: true
      estTime: ''
      isActive: true
      isTestData: false
      addedBy: 'system'
    console.log "Added Task %s", line[0]
    Tasks.insert task

Meteor.startup ->
  loadTasks line for line in initialTasks
  null
