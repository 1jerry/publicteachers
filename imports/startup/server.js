import { Migrations } from 'meteor/percolate:migrations'
// Setup code for server
import "./yaml.min"
import "./loadUsers"
import "./loadTables"

Meteor.startup(() => {
    Migrations.migrateTo('latest')
});
