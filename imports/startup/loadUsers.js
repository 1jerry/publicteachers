function loadUser(user) {
  var userAlreadyExists = !!Meteor.users.findOne({ username : user.username });

  let id;
  let profile;
  if (!userAlreadyExists) {
    console.info("add user ", user.username);
    id = Accounts.createUser(user);
    var updates = {}
    if (user.test) {
      updates.isTestData = true
    }
    if (user.name) {
      updates.name = user.name
    }
    if (user.number) {
      updates.number = String(user.number)
    }
    // more here in the future
    if (updates) {
      profile = Meteor.users.findOne(id).profile
      Object.assign(profile, updates)
      Meteor.users.update(id, {$set: {profile: profile}})
    }

  }
}

Meteor.startup(function () {
  var users = YAML.eval(Assets.getText('users.yml'));

  for (var key of Object.keys(users)) {
    loadUser(users[key]);
  }
});
