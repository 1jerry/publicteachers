Teachers' Brigade
----
This unfinished, working prototype allow teachers to connect with volunteers based on the task the teacher needs and the skill set of the volunteers.
The demo site is at [tb.1usa.us](https://tb.1usa.us/).  This is run as a container that goes to sleep after a while, so it might take a few seconds to spin up the first time each day.
You can log in as a teachers (teach1:test), or a volunteer (vol1:test), or a new user (test:test).  You can also sign-up for a new account, as a new user would.
Teachers create Jobs. The unfinished part: The teachers' view will show all *local* voluteers that match the skill.  The teacher would then choose to contact the volunteer to see if they want to work together.

## License
Copyright (c) 2018 by Integrity Calculation Systems

This was a speculative project I started in 2017 and stopped working on early 2018.  This is **not** a working version just because it is missing one required file.

This was written in JavaScript and CoffeeScript using [Meteor](https://www.meteor.com/ "Meteor home page").  Meteor is a full-stack isomorphic node.js platform using WebSockets and MongoDB for real-time data updates.  

It uses [Semantic-UI](https://semantic-ui.com/) as the style framework, and pre-processers [Stylus](http://stylus-lang.com/) and Jade.

Other common node.js packages used: fontawesome, moment, and lodash.